<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Post;

class PostsController extends Controller
{
    public function show($slug){
      // $posts = [
      //   'my-first-post' => 'hello, this is my first post',
      //   'my-second-post' => 'hello, this is my second post',
      //   'my-third-post' => 'hello, this is my third post'
      // ];
       $post = Post::where('slug', $slug)->firstOrFail();

        return view('post', [
          'post' => $post
        ]);
    }
}
