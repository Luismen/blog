<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Tag;

class ArticlesController extends Controller
{
    public function show(Article $article){
      //Shows a single article
      return view('articles.show',[ 'article'=> $article ]);
    }

    public function index(Article $article){
      //Render a list of articles
      if(request('tag')){
        $articles = Tag::where('name',request('tag'))->firstOrFail()->$articles;
        return $articles;
      }else{
        $articles = Article::latest()->get();
      }
      return view('articles.index',[ 'articles'=> $articles ]);
    }

    public function create(){
      //Shows a view to create a new resource
      $tags = Tag::all();
      return view('articles.create', [
           'tags' => $tags
      ]);
    }

    public function store(){
      //Persist the new resource
      //validation
      Article::create($this->validateArticle());

        return redirect(route('articles.index'));
    }

    public function edit(Article $article){
      //Show a view to edit an existing resource
      return view('articles.edit', compact('article'));
    }

    public function update(Article $article){
      $article->update($this->validateArticle());
      //Persist the edited resource
      //Find the article asociated with the id

      return redirect($article->path());

    }

    public function destroy(){
      //Delete the resource

    }

    protected function validateArticle(){
      return request()->validate([
        'title' => ['required', 'min:3', 'max:255'],
        'excerpt' => 'required',
        'body' => 'required'
      ]);
    }
}
