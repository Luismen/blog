<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    // public function getRouteKeyName(){
    //   return 'slug';//Article::where('slug',$article->first())
    // }

    protected $guarded = [];
    public function path(){
      return route('articles.show', $this);
    }

    public function author(){
      return $this->belongsTo(User::class, 'user_id');
    }

    public function tags(){
      return $this->belongsTo(Tag::class);
    }
}
